FROM python:3.7
ENV PYTHONUNBUFFERED 1

RUN mkdir -p /webapp
WORKDIR /webapp
COPY ./requirements.txt requirements.txt

RUN pip install --upgrade pip && \
    pip install -r requirements.txt && \
    pip install ipdb
