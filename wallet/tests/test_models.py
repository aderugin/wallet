from decimal import Decimal
from model_mommy import mommy
from django.test import TestCase

from wallet.exceptions import InsufficientFunds
from wallet.models import Wallet, WalletTransaction


class WalletTestCase(TestCase):
    def setUp(self):
        self.wallet = mommy.make(Wallet, amount=0)

    def test_make_deposit(self):
        transaction = self.wallet.make_deposit(100)
        self.assertEqual(self.wallet.amount, 0)
        transaction.complete()
        self.wallet.refresh_from_db()
        self.assertEqual(self.wallet.amount, 100)

    def test_make_withdrawal(self):
        self.assertRaises(InsufficientFunds, lambda: self.wallet.make_withdrawal(100))
        self.wallet.amount = 100
        self.wallet.save()
        self.assertRaises(InsufficientFunds, lambda: self.wallet.make_withdrawal(Decimal('100.01')))

        transaction = self.wallet.make_withdrawal(50)
        self.assertEqual(transaction.amount, -50)
        self.assertRaises(InsufficientFunds, lambda: self.wallet.make_withdrawal(Decimal('50.01')))
        self.wallet.refresh_from_db()
        self.assertEqual(self.wallet.amount, 50)

        transaction.complete()
        self.wallet.refresh_from_db()
        self.assertEqual(self.wallet.amount, 50)


class WalletTransactionTestCase(TestCase):
    def setUp(self):
        self.wallet = mommy.make(Wallet, amount=100)

    def test_complete_withdrawal(self):
        transaction = mommy.make(WalletTransaction, amount=-25, wallet=self.wallet)
        transaction.complete()
        self.wallet.refresh_from_db()
        self.assertEqual(self.wallet.amount, 100)

        transaction.refresh_from_db()
        transaction.complete()
        self.wallet.refresh_from_db()
        self.assertEqual(self.wallet.amount, 100)

    def test_complete_deposit(self):
        transaction = mommy.make(WalletTransaction, amount=25, wallet=self.wallet)
        transaction.complete()
        self.wallet.refresh_from_db()
        self.assertEqual(self.wallet.amount, 125)

        transaction.refresh_from_db()
        transaction.complete()
        self.wallet.refresh_from_db()
        self.assertEqual(self.wallet.amount, 125)

    def test_reject_withdrawal(self):
        transaction = mommy.make(WalletTransaction, amount=-25, wallet=self.wallet)
        transaction.reject()
        self.wallet.refresh_from_db()
        self.assertEqual(self.wallet.amount, 125)

        transaction.refresh_from_db()
        transaction.reject()
        self.wallet.refresh_from_db()
        self.assertEqual(self.wallet.amount, 125)

    def test_reject_deposit(self):
        transaction = mommy.make(WalletTransaction, amount=25, wallet=self.wallet)
        transaction.reject()
        self.wallet.refresh_from_db()
        self.assertEqual(self.wallet.amount, 100)

        transaction.refresh_from_db()
        transaction.reject()
        self.wallet.refresh_from_db()
        self.assertEqual(self.wallet.amount, 100)
