from django.db import models, transaction
from django.db.models import F
from django.contrib.auth import get_user_model

from .exceptions import InsufficientFunds


class Wallet(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.PROTECT)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    currency = models.CharField(max_length=3)

    @transaction.atomic
    def make_withdrawal(self, amount):
        wallet_transaction = WalletTransaction.objects.create(
            wallet=self,
            amount=-amount
        )
        count = self.__class__.objects.annotate(
            new_amount=F('amount') - amount
        ).filter(
            pk=self.pk,
            new_amount__gte=0
        ).update(
            amount=F('amount') - amount
        )
        # Также возможен вариант с добавлением условной проверки на поле
        # ну уровне postgres: ADD CHECK amount >= 0
        # и обработки ошибка IntegrityError
        if not count:
            raise InsufficientFunds
        return wallet_transaction

    def make_deposit(self, amount):
        return WalletTransaction.objects.create(
            wallet=self,
            amount=amount
        )


class WalletTransaction(models.Model):
    STATUS_PENDING = 0
    STATUS_COMPLETED = 1
    STATUS_REJECTED = 2

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    wallet = models.ForeignKey(Wallet, on_delete=models.PROTECT, related_name='transactions')
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    status = models.PositiveSmallIntegerField(default=STATUS_PENDING)

    @transaction.atomic
    def complete(self):
        if self.status != self.STATUS_PENDING:
            return None
        if self.is_deposit():
            Wallet.objects.filter(
                pk=self.wallet_id
            ).update(
                amount=models.F('amount') + self.amount
            )
        self.status = self.STATUS_COMPLETED
        self.save()

    @transaction.atomic
    def reject(self):
        if self.status != self.STATUS_PENDING:
            return None
        if not self.is_deposit():
            Wallet.objects.filter(
                pk=self.wallet_id
            ).update(
                amount=models.F('amount') + abs(self.amount)
            )
        self.status = self.STATUS_REJECTED
        self.save()

    def is_deposit(self):
        return self.amount > 0
