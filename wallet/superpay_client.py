def make_deposit(*, merchant_id, amount, currency, description=''):
    """
    Выполняет запрос к http://superpay.com/api/deposit
    """
    return {
        'redirect_url': 'http://superpay.com/some-user-interface-for-deposit?operation_id=foobar'
    }


def make_withdrawal(*, wallet_id, merchant_id, amount, currency, description=''):
    """
    Выполняет запрос к http://superpay.com/api/payout
    """
    return {
        'success': True,
        'error': None,
        'merchant_id': 42
    }


def get_transactions():
    """
    Получает список транзакций для реконселяции
    """
    return []


def decode_deposit_confirmation(value):
    # payload = do_some_stuff_for_decrypt(value, get_public_key())
    # return payload
    return {
        'merchant_id': 42,
        'amount': '100.5',
        'currency': 'USD',
        'created_at': '2019-06-05T00:00:00.000',
        'paid_at': '2019-06-05T00:00:00.000'
    }


def get_public_key():
    """
    Возвращает публичный ключ SHA256
    """
    pass
