from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError

from .models import WalletTransaction
from .exceptions import InsufficientFunds
from .serializers import BalanceOperationSerializer
from . import superpay_client


class BalanceOperationMixin(object):
    def get_balance_operation_data(self):
        serializer = BalanceOperationSerializer(
            data=self.request.data,
            context={'request': self.request}
        )
        serializer.is_valid(raise_exception=True)
        return serializer.validated_data['wallet'], serializer.validated_data['amount']


class WithdrawalView(BalanceOperationMixin, APIView):
    # authentication_classes = (SomeStrictAuthentication,)
    # permission_classes = (IsAuthenticated,)

    def post(self, request):
        wallet, amount = self.get_balance_operation_data()
        try:
            transaction = wallet.make_withdrawal(amount)
        except InsufficientFunds:
            raise ValidationError('Insufficient funds')
        data = superpay_client.make_withdrawal(
            wallet_id=wallet.id,
            merchant_id=transaction.id,
            amount=amount,
            currency=wallet.currency
        )
        if not data['success']:
            transaction.reject()
            return Response(status=400)
        transaction.complete()
        return Response(status=201)


class DepositView(BalanceOperationMixin, APIView):
    # authentication_classes = (SomeStrictAuthentication,)
    # permission_classes = (IsAuthenticated,)

    def post(self, request):
        wallet, amount = self.get_balance_operation_data()
        transaction = wallet.make_deposit(amount)
        data = superpay_client.make_deposit(
            merchant_id=transaction.id,
            amount=transaction.amount,
            currency=wallet.currency
        )
        return Response(data)


class DepositConfirmView(APIView):
    def post(self, request):
        data = superpay_client.decode_deposit_confirmation(request.data['payload'])
        transaction = WalletTransaction.objects.get(pk=data['merchant_id'])
        if transaction.amount != data['amount']:
            # Еще какие-то проверки
            transaction.reject()
            return Response({'code': 'some error code'}, status=400)
        transaction.complete()
        return Response({'code': 'some success code'})
