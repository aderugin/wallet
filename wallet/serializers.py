from decimal import Decimal
from rest_framework import serializers
from .models import Wallet


class BalanceOperationSerializer(serializers.Serializer):
    amount = serializers.DecimalField(max_digits=10, decimal_places=2, min_value=Decimal('0.01'))
    wallet = serializers.IntegerField()

    def validate_wallet(self, wallet_id):
        try:
            return Wallet.objects.get(
                pk=wallet_id,
                user=self.context['request'].user
            )
        except Wallet.DoesNotExist:
            raise serializers.ValidationError('Wallet does not exist')
